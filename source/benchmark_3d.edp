load "medit"
load "msh3"
include "cube.idp"
include "piecewise_function.idp"
include "tensor_math.idp"

// ***************
// Initialization.
// ***************
real startTime = clock(); // Get the clock in seconds.

// *********
// Settings.
// *********
int verbosity = 1; // 0 (no output) - >=4 (max output).
bool computeElasticPlasticThreshold = true;
real loadEpsilon = 1e-4;
real yieldEpsilon = 1e-6;
real minDu = 1e-8;
real NRSteps = 1e2;

if (verbosity > 0){
  cout << "*********************" << endl;
  cout << "Numerical tolerances." << endl;
  cout << "*********************" << endl;
  cout << "Compute elastic-plastic threshold: " << computeElasticPlasticThreshold << endl;
  cout << "Load loop tolerance: " << loadEpsilon << endl;
  cout << "Yield condition tolerance: " << yieldEpsilon << endl;
  cout << "Minimum displacement correction: " << minDu << endl;
  cout << "Max number of NR steps: " << NRSteps << endl;
}

// **************
// Generate mesh.
// **************
int[int] N = [20, 10, 5]; // Nodes per side.
real [int, int] B = [[0.0, 1.0], [0.0, 0.5], [0.0, 0.25]]; // Side lengths.
int [int, int] L = [[1, 2], [3, 4], [5, 6]]; // Labels.
mesh3 Th = Cube(N, B, L);
// Th = adaptmesh(Th, 1./n, IsMetric=1, nbvx=100000);
// plot(Th, wait=1, cmm="Mesh");
// assert(false);

// **********
// Variables.
// **********
real p = 70e6; // Pressure.
real youngModul = 2230e6; // Elasticity (Young) modulus.
real poisson = 0.4; // Poisson ratio.
real[int] matprops = [41.15e6, 0.0000,
                      49.26e6, 0.0003,
                      63.51e6, 0.0190,
                      67.72e6, 0.0340,
                      72.28e6, 0.0600,
                      76.69e6, 0.1000,
                      95.20e6, 0.2940];
// Lame parameters
real lam = youngModul * poisson / (1. - 2. * poisson) / (1. + poisson);
real G = youngModul / (2. * (1. + poisson)); // Shear modulus.
real bulk = youngModul / (3. * (1. - 2. * poisson)); // Bulk modulus.

if (verbosity > 0) {
  cout << "*******************" << endl;
  cout << "Physical constants." << endl;
  cout << "*******************" << endl;
  cout << "Young modulus: " << youngModul << endl;
  cout << "Poisson ratio: " << poisson << endl;
  cout << "Sigma(epsilon): " << matprops << endl;
  cout << "Lambda: " << lam << endl;
  cout << "Shear modulus: " << G << endl;
  cout << "Bulk modulus: " << bulk << endl;
}
// *********************
// Finite element space.
// *********************
fespace Stress(Th, P1);
fespace Strain(Th, P1);
fespace Residuum(Th, P1);
fespace Displacement(Th, P1);
fespace Nh(Th, [P1, P1, P1]);
// FEM variables.
Displacement u1, u2, u3, v1, v2, v3;
Stress sxx, syy, szz, sxy, sxz, syz;
Strain exx, eyy, ezz, exy, exz, eyz;
Strain pxx, pyy, pzz, pxy, pxz, pyz;
Strain epxx, epyy, epzz, epxy, epxz, epyz;
Residuum rx, ry, rz;

// **********
// Constants.
// **********
real third = 1. / 3.;

// *******
// Macros.
// *******
macro e(u1, u2, u3)  
  [
    dx(u1),
    dy(u2),
    dz(u3),
    (dy(u1) + dx(u2)) * 0.5,
    (dz(u1) + dx(u3)) * 0.5, 
    (dz(u2) + dy(u3)) * 0.5
  ] // EOM
macro sigma(u1, u2, u3)
	[
		(lam + 2. * G) * e(u1, u2, u3)[0] + lam * e(u1, u2, u3)[1] + lam * e(u1, u2, u3)[2],
		lam * e(u1, u2, u3)[0] + (lam + 2. * G) * e(u1, u2, u3)[1] + lam * e(u1, u2, u3)[2],
		lam * e(u1, u2, u3)[0] + lam * e(u1, u2, u3)[1] + (lam + 2. * G) * e(u1, u2, u3)[2],
		2. * G * e(u1, u2, u3)[3],
		2. * G * e(u1, u2, u3)[4],
		2. * G * e(u1, u2, u3)[5]
	] // EOM
macro stressdiv(Sxx, Syy, Szz, Sxy, Sxz, Syz) [dx(Sxx) + dy(Sxy) + dz(Sxz), 
                                               dx(Sxy) + dy(Syy) + dz(Syz),
                                               dx(Sxz) + dy(Syz) + dz(Szz)] // EOM

// ***********************
// Elastic-Plastic switch.
// ***********************
int loadSteps = 1e3;
int loadStepMax = loadSteps;
int loadStepMin = 0;
int loadTemp = int(loadStepMax / 2);
if (computeElasticPlasticThreshold){
  if (verbosity > 0) {
    cout << "Running bisection to locate Elastic-Plastic load switch." << endl;
  }

  real loadDiff = loadSteps;
  while (loadDiff > 1){
    real loadFraction = real(loadTemp) / loadSteps;

    varf elasticity([u1, u2, u3], [v1, v2, v3]) =
      int3d(Th)(sigma(u1, u2, u3)' * e(v1, v2, v3)) 
      + on(1, u1 = 0)
      + on(3, u2 = 0)
      + on(5, u3 = 0)
      ;
    varf l([u1, u2, u3], [v1, v2, v3]) = int2d(Th, 2)(loadFraction * p * v1)  
      + on(1, u1 = 0)
      + on(3, u2 = 0)
      + on(5, u3 = 0)
      ;

    matrix K = elasticity(Nh, Nh); // Stiffness matrix.
    real[int] b = l(0, Nh); // External forces.
    real[int] U(K.n);
    // Solve.
    U = K^-1 * b;
    for(int i = 0; i < K.n / 3; i++){
        u1[](i) = U(3 * i + 0);
        u2[](i) = U(3 * i + 1);
        u3[](i) = U(3 * i + 2);
    }

    exx = e(u1, u2, u3)[0];
    eyy = e(u1, u2, u3)[1];
    ezz = e(u1, u2, u3)[2];
    exy = e(u1, u2, u3)[3];
    exz = e(u1, u2, u3)[4];
    eyz = e(u1, u2, u3)[5];
    bool isPlastic = false;

    for (int i = 0; i < Th.nv; i++){
      real volStrain = exx[](i) + eyy[](i) + ezz[](i);
      real[int] devStress(6); // Deviatoric stress.
      devStress = 2. * G * [exx[](i) - third * volStrain, 
                            eyy[](i) - third * volStrain,
                            ezz[](i) - third * volStrain,
                            exy[](i),
                            exz[](i),
                            eyz[](i)
                            ];

      real q = sqrt(1.5 * (pow(devStress[0], 2) + 
                           pow(devStress[1], 2) + 
                           pow(devStress[2], 2) + 
                           2. * pow(devStress[3], 2) + 
                           2. * pow(devStress[4], 2) + 
                           2. * pow(devStress[5], 2))
                   );
    
      // Check yield stress condition.
      real sigmaYield = matprops(0);
      real yieldCriteria = q - sigmaYield;
      isPlastic = yieldCriteria > 0;

      if (isPlastic){
        break;
      }
    }

    if (isPlastic) {
      loadDiff = loadTemp;
      loadStepMax = loadTemp;
      loadTemp = int(0.5 * (loadStepMax + loadStepMin));
      loadDiff = loadDiff - loadTemp;
    }
    else {
      loadDiff = loadTemp;
      loadStepMin = loadTemp;
      loadTemp = int(0.5 * (loadStepMin + loadStepMax));
      loadDiff = loadTemp - loadDiff;
    }
  }
  if (verbosity > 1) {
    cout << "  Load switch at: " << loadTemp  << " out of " << loadSteps << "." << endl;
  }
}

loadTemp = computeElasticPlasticThreshold ? loadTemp : 1;

// **********
// Load loop.
// **********
if (verbosity > 0) {
  cout << "Computation started with " << loadSteps << " load steps." << endl;
}
real[int] epsPlastic(Th.nv);
for (int loadStep = loadTemp; loadStep <= loadSteps; loadStep++){
  bool isPlasticStep = false;
  real loadFraction = real(loadStep) / loadSteps;
  if (verbosity > 1) {
    cout << "======================" << endl;
    cout << "Load factor: " << loadFraction << "." << endl; 
    cout << "======================" << endl;
  }

  // *************************************
  // Variational definition of elasticity.
  // *************************************
  varf elasticity([u1, u2, u3], [v1, v2, v3]) =
	  int3d(Th)(sigma(u1, u2, u3)' * e(v1, v2, v3)) 
      + on(1, u1 = 0)
      + on(3, u2 = 0)
      + on(5, u3 = 0)
    ;

  varf l([u1, u2, u3], [v1, v2, v3]) = int2d(Th, 2)(loadFraction * p * v1)  
      + on(1, u1 = 0)
      + on(3, u2 = 0)
      + on(5, u3 = 0)
    ;

  // *****************
  // Solve elasticity.
  // *****************
  if (verbosity > 2) {
    cout << "  Assuming elastic step as initial guess." << endl;
  }
  matrix K = elasticity(Nh, Nh); // Stiffness matrix.
  real[int] b = l(0, Nh); // External forces.
  real[int] U(K.n);
  // Solve.
  U = K^-1 * b;
  // Convert arrays to fespace variables.
  for(int i = 0; i < K.n / 3; i++){
      u1[](i) = U(3 * i + 0);
      u2[](i) = U(3 * i + 1);
      u3[](i) = U(3 * i + 2);
  }
  if (verbosity > 3) {
    cout << "    Max elastic u1: " << u1[].max << endl;
    cout << "    Min elastic u2: " << u2[].min << endl;
    cout << "    Min elastic u3: " << u3[].min << endl;
  }

  // **********************
  // Plasticity correction.
  // **********************
  if (verbosity > 2) {
    cout << "  Plasticity correction." << endl;
  }
  int k = 0;
  while (k <= NRSteps){
    if (verbosity > 2) {
      cout << "  Iteration step: " << k << "." << endl;
      cout << "    Computing residuum ..." << endl;
    }
    // Compute stresses.  
    sxx = sigma(u1, u2, u3)[0];
    syy = sigma(u1, u2, u3)[1]; 
    szz = sigma(u1, u2, u3)[2]; 
    sxy = sigma(u1, u2, u3)[3]; 
    sxz = sigma(u1, u2, u3)[4];
    syz = sigma(u1, u2, u3)[5];
    // Compute residuum.
    rx = stressdiv(sxx, syy, szz, sxy, sxz, syz)[0];
    ry = stressdiv(sxx, syy, szz, sxy, sxz, syz)[1];
    rz = stressdiv(sxx, syy, szz, sxy, sxz, syz)[2];
    if (verbosity > 3) {
      cout << "      rx" << endl;
      cout << "      " << rx[].max << endl;
      cout << "      " << rx[].min << endl;
      cout << "      ry" << endl;
      cout << "      " << ry[].max << endl;
      cout << "      " << ry[].min << endl;
      cout << "      rz" << endl;
      cout << "      " << rz[].max << endl;
      cout << "      " << rz[].min << endl;
    }

    // **********
    // Obtain du.
    // **********
    if (verbosity > 2) {
      cout << "    Obtaining dU ..." << endl;
    }
    real[int] dU(K.n);
    for (int i = 0; i < rx.n; i++){
      b(3 * i + 0) = -rx[](i);
      b(3 * i + 1) = -ry[](i);
      b(3 * i + 2) = -rz[](i);
    }
    dU = K^-1 * b;
    if (verbosity > 3) {
      cout << "      dU max: " << dU.max << endl;
      cout << "      dU min: " << dU.min << endl;
    }

    // *************************
    // Compute new displacement.
    // *************************
    if (verbosity > 2) {
      cout << "    Computing new displacement field ..." << endl;
    }
    for (int i = 0; i < u1.n; i++){
      real dux = dU(3 * i + 0);
      real duy = dU(3 * i + 1);
      real duz = dU(3 * i + 2);

      u1[](i) = u1[](i) + (abs(dux) >= minDu ? dux : 0.0);
      u2[](i) = u2[](i) + (abs(duy) >= minDu ? duy : 0.0);
      u3[](i) = u3[](i) + (abs(duz) >= minDu ? duz : 0.0);
    }
    if (verbosity > 3) {
      cout << "      u1 max: " << u1[].max << endl;
      cout << "      u2 min: " << u2[].min << endl;
      cout << "      u3 min: " << u3[].min << endl;
    }

    // *************************
    // Compute modified strains.
    // *************************
    if (verbosity > 2) {
      cout << "    Computing new strains ..." << endl;
    }
    exx = e(u1, u2, u3)[0];
    eyy = e(u1, u2, u3)[1];
    ezz = e(u1, u2, u3)[2];
    exy = e(u1, u2, u3)[3];
    exz = e(u1, u2, u3)[4];
    eyz = e(u1, u2, u3)[5];

    // ******************
    // Stress correction.
    // ******************
    if (verbosity > 2) {
      cout << "    Stress correction." << endl;
    }
    for (int i = 0; i < Th.nv; i++){
      real volStrain = exx[](i) + eyy[](i) + ezz[](i);
      real[int] devStress(6); // Deviatoric stress.
      devStress = 2. * G * [exx[](i) - third * volStrain, 
                            eyy[](i) - third * volStrain,
                            ezz[](i) - third * volStrain,
                            exy[](i),
                            exz[](i),
                            eyz[](i)
                            ];

      real q = sqrt(1.5 * (pow(devStress[0], 2) + 
                           pow(devStress[1], 2) + 
                           pow(devStress[2], 2) + 
                           2. * pow(devStress[3], 2) + 
                           2. * pow(devStress[4], 2) + 
                           2. * pow(devStress[5], 2))
                   );
    
      // Check yield stress condition.
      real sigmaYield = plfun(epsPlastic(i), matprops.n / 2, matprops);
      real yieldCriteria = q - sigmaYield;
      if (yieldCriteria > 0){
        // Yield condition exceeded. Correction required.
        if (!isPlasticStep && verbosity > 2){
          cout << "      ********************************" << endl;
          cout << "      Plastic deformation encountered." << endl;
          cout << "      ********************************" << endl;
        }
        isPlasticStep = true;

        real dgama = 0.0; // Initial gamma guess.
        real convergenceCheck = yieldCriteria;
        int correctionStep = 0;
        while (abs(convergenceCheck) > yieldEpsilon && correctionStep < NRSteps){
          real epsPlasticDgama = epsPlastic(i) + dgama;
          
          real H = dplfun(epsPlasticDgama, matprops.n / 2, matprops);
          real d = -3. * G - H;
          dgama = dgama - convergenceCheck / d;

          // Check for convergence.
          sigmaYield = plfun(epsPlastic(i) + dgama, matprops.n / 2, matprops);
          convergenceCheck = q - 3. * G * dgama - sigmaYield;

          correctionStep = correctionStep + 1;
        }
        assert(correctionStep < NRSteps); // Max correction steps at point i exceeded."

        // Update state variables.
        // Update stress.
        real volStress = bulk * volStrain;
        real correctionFactor = (1. - dgama * 3. * G / q);
        devStress = correctionFactor * devStress;
        sxx[](i) = volStress + devStress[0];
        syy[](i) = volStress + devStress[1];
        szz[](i) = volStress + devStress[2];
        sxy[](i) = devStress[3];
        sxz[](i) = devStress[4];
        syz[](i) = devStress[5];
        // Update strain.
        exx[](i) = third * volStrain + devStress[0] / (2. * G);
        eyy[](i) = third * volStrain + devStress[1] / (2. * G);
        ezz[](i) = third * volStrain + devStress[2] / (2. * G);
        exy[](i) = devStress[3] / (2. * G);
        exz[](i) = devStress[4] / (2. * G);
        eyz[](i) = devStress[5] / (2. * G);
        // Update plastic strain scalar.
        epsPlastic(i) = epsPlastic(i) + dgama;
        // Update plastic strain.
        real plasticStrainFactor = dgama * sqrt(1.5) / GetSixvectorNorm(devStress);
        pxx[](i) = pxx[](i) + plasticStrainFactor * devStress[0];
        pyy[](i) = pyy[](i) + plasticStrainFactor * devStress[1];
        pzz[](i) = pzz[](i) + plasticStrainFactor * devStress[2];
        pxy[](i) = pxy[](i) + plasticStrainFactor * devStress[3];
        pxz[](i) = pxz[](i) + plasticStrainFactor * devStress[4];
        pyz[](i) = pyz[](i) + plasticStrainFactor * devStress[5];
      }
    }
    
    // ****************************
    // Re-checking internal forces.
    // ****************************
    if (verbosity > 1) {
      cout << "    Checking load step coonvergence ..." << endl;
    }
    rx = stressdiv(sxx, syy, szz, sxy, sxz, syz)[0];
    ry = stressdiv(sxx, syy, szz, sxy, sxz, syz)[1];
    rz = stressdiv(sxx, syy, szz, sxy, sxz, syz)[2];
    if (verbosity > 3) {
      cout << "      rx" << endl;
      cout << "      " << rx[].max << endl;
      cout << "      " << rx[].min << endl;
      cout << "      ry" << endl;
      cout << "      " << ry[].max << endl;
      cout << "      " << ry[].min << endl;
      cout << "      rz" << endl;
      cout << "      " << rz[].max << endl;
      cout << "      " << rz[].min << endl;
    }

    // Compute norm of residuum.
    real norm2 = 0;
    for (int i = 0; i < rx.n; i++){
      norm2 = pow(rx[](i), 2) + pow(ry[](i), 2) + pow(rz[](i), 2);
    }
    if (verbosity > 3) {
      cout << "      ||r|| = " << sqrt(norm2) << endl;
    }
    if (sqrt(norm2) < loadEpsilon){
      break;
    }
    k = k + 1;
  }
  assert(k < NRSteps); // Maximum iteration k exceeded.
}

// ********
// Results.
// ********
// Compute stresses. 
sxx = sigma(u1, u2, u3)[0];
syy = sigma(u1, u2, u3)[1]; 
szz = sigma(u1, u2, u3)[2]; 
sxy = sigma(u1, u2, u3)[3]; 
sxz = sigma(u1, u2, u3)[4];
syz = sigma(u1, u2, u3)[5];
cout << "========" << endl;
cout << "Results:" << endl;
cout << "========" << endl;
cout << "sxx" << endl;
cout << "  " << sxx[].max << endl;
cout << "  " << sxx[].min << endl;
cout << "syy" << endl;
cout << "  " << syy[].max << endl;
cout << "  " << syy[].min << endl;
cout << "szz" << endl;
cout << "  " << szz[].max << endl;
cout << "  " << szz[].min << endl;
cout << "sxy" << endl;
cout << "  " << sxy[].max << endl;
cout << "  " << sxy[].min << endl;
cout << "sxz" << endl;
cout << "  " << sxz[].max << endl;
cout << "  " << sxz[].min << endl;
cout << "syz" << endl;
cout << "  " << syz[].max << endl;
cout << "  " << syz[].min << endl;
// Compute strains.
exx = e(u1, u2, u3)[0];
eyy = e(u1, u2, u3)[1];
ezz = e(u1, u2, u3)[2];
exy = e(u1, u2, u3)[3];
exz = e(u1, u2, u3)[4];
eyz = e(u1, u2, u3)[5];
cout << "exx" << endl;
cout << "  " << exx[].max << endl;
cout << "  " << exx[].min << endl;
cout << "eyy" << endl;
cout << "  " << eyy[].max << endl;
cout << "  " << eyy[].min << endl;
cout << "ezz" << endl;
cout << "  " << ezz[].max << endl;
cout << "  " << ezz[].min << endl;
cout << "exy" << endl;
cout << "  " << exy[].max << endl;
cout << "  " << exy[].min << endl;
cout << "exz" << endl;
cout << "  " << exz[].max << endl;
cout << "  " << exz[].min << endl;
cout << "eyz" << endl;
cout << "  " << eyz[].max << endl;
cout << "  " << eyz[].min << endl;
cout << "pxx" << endl;
cout << "  " << pxx[].max << endl;
cout << "  " << pxx[].min << endl;
cout << "pyy" << endl;
cout << "  " << pyy[].max << endl;
cout << "  " << pyy[].min << endl;
cout << "pzz" << endl;
cout << "  " << pzz[].max << endl;
cout << "  " << pzz[].min << endl;
cout << "pxy" << endl;
cout << "  " << pxy[].max << endl;
cout << "  " << pxy[].min << endl;
cout << "pxz" << endl;
cout << "  " << pxz[].max << endl;
cout << "  " << pxz[].min << endl;
cout << "pyz" << endl;
cout << "  " << pyz[].max << endl;
cout << "  " << pyz[].min << endl;
// Total strain.
for (int i = 0; i < pxx.n; i++){
  epxx[](i) = pxx[](i) + exx[](i);
  epyy[](i) = pyy[](i) + eyy[](i);
  epzz[](i) = pzz[](i) + ezz[](i);
  epxy[](i) = pxy[](i) + exy[](i);
  epxz[](i) = pxz[](i) + exz[](i);
  epyz[](i) = pyz[](i) + eyz[](i);
}
cout << "Total strain xx" << endl;
cout << "  " << epxx[].max << endl;
cout << "  " << epxx[].min << endl;
cout << "Total strain yy" << endl;
cout << "  " << epyy[].max << endl;
cout << "  " << epyy[].min << endl;
cout << "Total strain zz" << endl;
cout << "  " << epzz[].max << endl;
cout << "  " << epzz[].min << endl;
cout << "Total strain xy" << endl;
cout << "  " << epxy[].max << endl;
cout << "  " << epxy[].min << endl;
cout << "Total strain xz" << endl;
cout << "  " << epxz[].max << endl;
cout << "  " << epxz[].min << endl;
cout << "Total strain yz" << endl;
cout << "  " << epyz[].max << endl;
cout << "  " << epyz[].min << endl;
// Displacements.
cout << "      u1 max: " << u1[].max << endl;
cout << "      u2 min: " << u2[].min << endl;
cout << "      u3 min: " << u3[].min << endl;

cout << "Computation finished. Elapsed time: " << (clock() - startTime) / 60.0 << " minutes." << endl;
